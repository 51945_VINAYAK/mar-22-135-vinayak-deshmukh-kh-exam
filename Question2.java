class SinglyList{
Node head;
Node head2;


class Node
{
int data;
Node next;

 public Node(int data){
 this.data=data;
 this.next=null;
}
}

public void insertNode(int data){
Node newnode=new Node(data);
if(head==null){
head=newnode;
return;
}
Node temp=head;
while(temp.next!=null){
temp=temp.next;
}
temp.next=newnode;
}
public void displayList(){
Node temp=head;
if(head==null){
System.out.println("list is empty!!!");
return;
}
while(temp!=null){
System.out.print(temp.data+"-->");
temp=temp.next;
}
}



public void reverse(Node head){
	Node temp=head;
	if(temp.next==null){
		System.out.print(temp.data+"-->");
		return;
	}
	
	reverse(temp.next);
	System.out.print(temp.data+"-->");
	
}
}

public class Question2{
public static void main(String args[]){
SinglyList s=new SinglyList();
s.insertNode(1);
s.insertNode(5);
s.insertNode(1);
s.insertNode(2);
s.insertNode(3);
s.insertNode(4);
s.insertNode(5);
s.displayList();
System.out.println();
s.reverse(s.head);

}

}